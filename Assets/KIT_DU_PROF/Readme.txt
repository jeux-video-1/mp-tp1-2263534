Fonts =============================================================
• La typographie a utiliser partout où vous afficher du texte à l'écran


Prefabs ===========================================================
• Crane --------------
	Prefab à faire apparaitre lorsqu'un ennemi meurt. Il faut faire disparaitre l'ennemi et faire apparaitre le crane à sa place

• Explosion ----------
	Prefab à faire apparaitre lorsqu'une explosion à lieu. Il ne s'agit que d'un effet visuel, tout le code pour les dégats reste à faire

• Indicateur Dégats --
	Prefab qui afficher les dégats subits. Il faut instantier le prefab et appeler la fonction Setup(float degats) du script IndicateurDegats.cs

• Toki ---------------
	Prefab du chien (Toki). Il faut lui assigner la variable joueur. C'est la cible que Toki suivra. Lors de la réapparition du joueur après sa mort, il faut appeler joueurEstReapparu() du script Toki.cs pour le faire réapparaitre lui aussi.

• Trouvailles --------
	Prefabs des objets que Toki peut déterrer. Il faudra modifier la potion pour qu'elle puisse être récupérer par le joueur.

• Barre de vie -------
	Prefab de la barre de vie. Il faudra créer un script pour la gérer. 


Scripts ===========================================================
• Dynamic Sprite Sorting ---
	Permet de modifier le SortingOrder des sprites en temps-réel pour permettre à certains sprites d'apparaitre devant ou derrière d'autre selon leur position.
	- LayerOrderStatic: 	À appliquer sur les objets qui sont immobiles
	- LayerOrderDynamic: 	À appliquer sur les objets qui se déplacent

• AutomaticSpriteFlip
	Permet de faire le flip de direction des sprites dépendemment de leur direction. Il faut instantier cette classe et appeler la fonction SetDirection() selon la direction où l'on veut que le sprite regarde


SFX ===============================================================
	Contient les différents effets sonores nécessaire au jeu


Sprites ===========================================================
	Contient les différents sprites nécessaire au jeu


Tilesets ==========================================================
	Contient les différents Tilesets nécessaire au jeu