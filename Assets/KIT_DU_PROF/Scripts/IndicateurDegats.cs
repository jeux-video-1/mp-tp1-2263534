using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IndicateurDegats : MonoBehaviour
{
    TextMeshPro textMesh;

    Vector3 vecteur;

    public void Setup(float degats)
    {
        textMesh = GetComponent<TextMeshPro>();

        // Convertir en int pour �viter les d�cimales
        textMesh.text = Mathf.RoundToInt(degats).ToString();

        // Direction de l'animation
        vecteur.x = Random.Range(-1f, 1f);
        vecteur.y = 1f;
        vecteur.z = 0f;

        // Destruction apr�s un d�lai
        Destroy(gameObject, 0.5f);
    }

    void Update()
    {
        transform.Translate(vecteur * 3f * Time.deltaTime);
    }
}
