using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotator : MonoBehaviour
{
    [SerializeField] float rotation;

    void Update()
    {
        transform.Rotate(Vector3.forward * rotation * Time.deltaTime);
    }
}
