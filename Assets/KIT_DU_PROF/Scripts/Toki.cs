using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Toki : MonoBehaviour
{
    [Header("Le transform du joueur qui sera suivi")]
    [SerializeField] Transform joueur;

    [Header("Prefabs")]
    public GameObject prefabTrouvailleParent;
    public GameObject[] prefabsJunk;
    public GameObject prefabPotion;

    Animator animator;

    // Compte la distance parcouru
    float distanceParcouru;
    Vector3 dernierePosition;
    const float distanceNecessaire = 100f;

    // Compteur de trous
    int cptTrous;

    AudioSource source;
    NavMeshAgent agent;
    AutomaticSpriteFlip autoSpriteFlip;


    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        source = GetComponent<AudioSource>();
        agent = GetComponent<NavMeshAgent>();
        autoSpriteFlip = new AutomaticSpriteFlip(transform);

        InvokeRepeating("SuivreJoueur", 1f, 0.5f);
    }

    void Update()
    {

        // Calculer la distance parcourue
        float distance = Vector3.Distance(dernierePosition, transform.position);
        distanceParcouru += distance;

        // Animation de mouvement
        animator.SetBool("isMoving", agent.velocity.magnitude > 0.5f);

        // Ajuster la derni�re position
        dernierePosition = transform.position;

        if (distanceParcouru > distanceNecessaire)
        {
            StartCoroutine(Creuser());

            // Reset de la distance
            distanceParcouru = 0f;
        }

        // Ajuster la direction du sprite
        if (agent.velocity.x < 0f)
            autoSpriteFlip.SetDirection(AutomaticSpriteFlip.Direction.gauche);

        else if (agent.velocity.x > 0f)
            autoSpriteFlip.SetDirection(AutomaticSpriteFlip.Direction.droite);
    }

    void SuivreJoueur()
    {
        // Suivre le joueur
        if (joueur)
            agent.SetDestination(joueur.position);
    }

    IEnumerator Creuser()
    {
        // Arr�ter le d�placement
        agent.isStopped = true;

        // Animation
        animator.SetTrigger("bark");

        // Effet sonore
        source.Play();

        // Attendre avant de spawn l'objet
        yield return new WaitForSeconds(0.5f);

        GameObject trouvailleParent = Instantiate(prefabTrouvailleParent, transform.position, Quaternion.identity);

        GameObject prefabTrouvaille = null;

        switch (cptTrous)
        {
            case 2:
                prefabTrouvaille = prefabPotion;
                cptTrous = 0;
                break;
            default:
                prefabTrouvaille = prefabsJunk[Random.Range(0, prefabsJunk.Length)];
                cptTrous++;
                break;
        }

        // Instantie la trouvaille
        GameObject trouvaille = Instantiate(prefabTrouvaille, transform.position, Quaternion.identity);

        // Mettre la trouvaille dans le parent
        trouvaille.transform.parent = trouvailleParent.transform.GetChild(0);

        Destroy(trouvailleParent, 30f);

        // Attendre avant de recommencer � bouger
        yield return new WaitForSeconds(0.5f);

        // R�activer le d�placement
        agent.isStopped = false;

    }

    // Appel� lors de la mort du joueur pour faire r�apparaitre Toki
    public void joueurEstReapparu()
    {
        // Teleporter � la nouvelle position du joueur
        agent.Warp(joueur.position);

        // Ajuster la derni�re position
        dernierePosition = joueur.position;

        // Reset de la distance parcouru
        distanceParcouru = 0f;

        // Reset du compteur
        cptTrous = 0;
    }
}
