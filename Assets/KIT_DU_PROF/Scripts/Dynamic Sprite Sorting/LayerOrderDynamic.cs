using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LayerOrder
{

    /// <summary>
    /// G�re le OrderInLayer d'un SpriteRenderer
    /// NE DOIT �TRE APPLIQU� QUE SUR LES OBJETS QUI PEUVENT SE D�PLACER
    /// </summary>
    public class LayerOrderDynamic : MonoBehaviour
    {
        SpriteRenderer spriteRenderer;

        void Start()
        {
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }

        void Update()
        {
            LayerOrderSetter.UpdateOrder(transform, spriteRenderer);
        }
    }
}
