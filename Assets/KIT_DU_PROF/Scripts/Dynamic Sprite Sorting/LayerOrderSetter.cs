using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LayerOrder
{
    public static class LayerOrderSetter
    {
        public static void UpdateOrder(Transform transform, SpriteRenderer spriteRenderer)
        {
            spriteRenderer.sortingOrder = Mathf.RoundToInt(transform.position.y * 10f) * -1;

        }
    }
}