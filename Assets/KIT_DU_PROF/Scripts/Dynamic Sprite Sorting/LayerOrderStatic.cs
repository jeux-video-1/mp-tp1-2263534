using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LayerOrder
{
    /// <summary>
    /// G�re le OrderInLayer d'un SpriteRenderer
    /// NE DOIT �TRE APPLIQU� QUE SUR LES OBJETS QUI NE PEUVENT PAS SE D�PLACER
    /// </summary>
    public class LayerOrderStatic : MonoBehaviour
    {
        SpriteRenderer spriteRenderer;

        void Start()
        {
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();

            LayerOrderSetter.UpdateOrder(transform, spriteRenderer);
        }
    }

}